#Remove files from previous pipeline executions
rm data/*.*
rm out/merged/*.gz
rm out/trimmed/*.*
rm res/contaminants.*
rm log/cutadapt/*.log
rm out/star/C57BL/*.*
rm out/star/SPRET/*.*

#Download all the files specified in data/filenames
while read -r url
do
  scripts/download.sh $url data
done < data/urls

# Download the contaminants fasta file, and uncompress it
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes

# Index the contaminants file
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx

# Merge the samples into a single file
for sid in $(ls data/*.fastq.gz | cut -d "_" -f1 | cut -d "/" -f2 | sort | uniq) #TODO
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done

# run cutadapt for all merged files
for sid in $(ls data/*.fastq.gz | cut -d "_" -f1 | cut -d "/" -f2 | sort | uniq) #TODO
do
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/$sid.trimmed.fastq.gz out/merged/$sid.fastq.gz > log/cutadapt/$sid.log
done

    # you will need to obtain the sample ID from the filename
    for sid in $(ls data/*.fastq.gz | cut -d "_" -f1 | cut -d "/" -f2 | sort | uniq)
    do
         mkdir -p out/star/$sid
    done

# run STAR for all trimmed files
for fname in out/trimmed/*.fastq.gz
do
     SAMPLE=$(basename $fname .trimmed.fastq.gz)
     STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn \
        $fname --readFilesCommand gunzip -c --outFileNamePrefix out/star/$SAMPLE/
done

# Create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
echo ----------------------------- >> log.out
date >> log.out
echo ----------------------------- >> log.out
echo cutadapt C57BL >> log.out
grep "Reads with adapters" log/cutadapt/C57BL.log >> log.out
grep "Total basepairs" log/cutadapt/C57BL.log >> log.out
echo cutadapt SPRET  >> log.out
grep "Reads with adapters" log/cutadapt/SPRET.log >> log.out
grep "Total basepairs" log/cutadapt/SPRET.log >> log.out
echo star C57BL >> log.out
grep "Uniquely mapped reads %" out/star/C57BL/Log.final.out >> log.out
grep "% of reads mapped to multiple loci" out/star/C57BL/Log.final.out >> log.out
grep "% of reads mapped to too many loci" out/star/C57BL/Log.final.out >> log.out
echo star SPRET >> log.out
grep "Uniquely mapped reads %" out/star/SPRET/Log.final.out >> log.out
grep "% of reads mapped to multiple loci" out/star/SPRET/Log.final.out >> log.out
grep "% of reads mapped to too many loci" out/star/SPRET/Log.final.out >> log.out
