# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".

WD=$(pwd)

if [ "$#" -eq 2 ]
then
    cd $2
    wget $1
    cd $WD
elif [ "$#" -eq 3 ]
then
    cd $2
    wget $1
    if [ $3 == "yes" ]
    then
	FICHZIP=$(basename $1)
	gunzip -k $FICHZIP
	cd $WD
    fi
else
    echo "Usage: $0 <fileURL> <directory> [yes for gunzip]"
    exit 1
fi



