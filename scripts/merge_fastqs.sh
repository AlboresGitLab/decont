# This script should merge all files from a given sample (the sample id is provided in the third argument ($3))
# into a single file, which should be stored in the output directory specified by the second argument ($2).
# The directory containing the samples is indicated by the first argument ($1).
HOMEDIR=$(pwd)

#directorio donde estan los samples
#echo $1
#ls $1

#directorio de salida out/merged
#echo $2
#ls $2

#sample IDS
#echo SampleIds $3


cd $1
cat $(ls | grep $3) > $3.fastq.gz
mv $3.fastq.gz $HOMEDIR'/'$2
